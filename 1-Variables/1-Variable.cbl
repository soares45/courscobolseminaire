      ******************************************************************
      * Author: David Gilbert
      * Date: 17 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. VARIABLE-EXEMPLE.
       DATA DIVISION.

      *     Initialisation des variables
       WORKING-STORAGE SECTION.
      ******************************************************************
      *    77 : Variable seule

      *     9 : Variable de type Numerique
      *     A : Variable Alphabetique
      *     X : Variable Alphanumerique

      *     Le nombre a cote signifie le nombre de Chiffres/Characteres
      *       la variable peut avoir au maximum
      ******************************************************************
           77 NOMBREVERSION2 PIC 9(3).
           77 NOMBREVIRGULE  PIC 9(3)v9(2).

           77 NOMBRE          PIC 999.
           77 CHENECARACTERE  PIC X(3).
           77 JUSTECHARACTERE PIC A(3).

       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY NOMBRE "|" NOMBREVERSION2 "|" NOMBREVIRGULE.
           DISPLAY CHENECARACTERE "|" JUSTECHARACTERE.

      *     Les deux sont equivalents
           SET NOMBRE TO 111.
      *     MOVE  111  TO NOMBRE.

           MOVE 'ASD' TO CHENECARACTERE
           MOVE 'ASD' TO JUSTECHARACTERE
           DISPLAY NOMBRE "|" CHENECARACTERE "|" JUSTECHARACTERE.

      *      Il est non conseiller de mettre des lettre dans un nombre
      *        et vice-versa
      *     MOVE 'ZZZ' TO NOMBRE
      *     MOVE  111  TO CHENECARACTERE
      *     MOVE  111  TO JUSTECHARACTERE.

           DISPLAY NOMBRE "|" CHENECARACTERE
            STOP RUN.
       END PROGRAM VARIABLE-EXEMPLE.
