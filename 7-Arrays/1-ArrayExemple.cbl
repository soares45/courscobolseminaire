      ******************************************************************
      * Author: David Gilbert
      * Date: 20 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *     Exemple de comment creer un tableau en COBOL
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. EXEMPLE-ARRAY.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      *    ***********************************************
      *     Permet d'utiliser
           01 TAB-DE-LA-SEMAINE.
               05 NOM-JOURNEE PIC X(9) OCCURS 7 TIMES.
      *    Au lieu de
      *    01 TABLE-DE-LA-SEMAINE
      *        05 NOM-JOURNEE-1 PIC X(8).
      *        05 NOM-JOURNEE-2 PIC X(8).
      *        05 NOM-JOURNEE-3 PIC X(8).
      *        05 NOM-JOURNEE-4 PIC X(8).
      *        05 NOM-JOURNEE-5 PIC X(8).
      *        05 NOM-JOURNEE-6 PIC X(8).
      *        05 NOM-JOURNEE-7 PIC X(8).
      *    ***********************************************

       PROCEDURE DIVISION.
      *        Initialise le tableau
            MOVE "LUNDI"    TO NOM-JOURNEE(1).
            MOVE "MARDI"    TO NOM-JOURNEE(2).
            MOVE "MERCREDI" TO NOM-JOURNEE(3).
            MOVE "JEUDI"    TO NOM-JOURNEE(4).
            MOVE "VENDREDI" TO NOM-JOURNEE(5).
            MOVE "SAMEDI"   TO NOM-JOURNEE(6).
            MOVE "DIMANCHE" TO NOM-JOURNEE(7).

      *        Afficher le tableau
            DISPLAY TAB-DE-LA-SEMAINE.
            DISPLAY NOM-JOURNEE(3).
            STOP RUN.
       END PROGRAM EXEMPLE-ARRAY.
