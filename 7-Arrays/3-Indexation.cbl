      ******************************************************************
      * Author: David Gilbert
      * Date: 20 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. INDEXATION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           01 TABLEAU.
               05 VALEUR PIC X(2) OCCURS 5 TIMES INDEXED BY INDX.
           77 I PIC 9.
       PROCEDURE DIVISION.
            SET INDX TO 1.
            SET I TO 0.
            PERFORM BOUCLE UNTIL INDX > 5.
            DISPLAY TABLEAU.
            STOP RUN.
       BOUCLE.
           MOVE I TO VALEUR(INDX)
           SET INDX UP BY 1.
           SET I UP BY 1.

       END PROGRAM INDEXATION.
