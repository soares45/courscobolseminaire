      ******************************************************************
      * Author: David Gilbert
      * Date: 21 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Permet de fusionner des chaines de caracteres en une seule
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. FUSION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           77 STRING1 PIC X(15) VALUE 'APPRENONS / POM'.
           77 STRING2 PIC X(11) VALUE 'LA BASE DU'.
           77 STRING3 PIC X(30) VALUE 'COBOL STRING .....'.

           77 STRING4 PIC X(50).
           77 EXEMPLEPOINTER PIC 9(2).
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
            MOVE 1 TO EXEMPLEPOINTER.
            STRING STRING1 DELIMITED BY '/'
                   STRING2 DELIMITED BY SIZE
                   STRING3 DELIMITED BY SPACE
               INTO STRING4
      *    POINTER sert a indiquer ou il commence a mettre les informations
      *             WITH POINTER EXEMPLEPOINTER
            END-STRING.
            DISPLAY STRING4.
            DISPLAY EXEMPLEPOINTER.
            STOP RUN.
       END PROGRAM FUSION.
