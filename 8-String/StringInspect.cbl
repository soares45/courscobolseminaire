      ******************************************************************
      * Author: David Gilbert
      * Date: 21 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Permet de rechercher et modifier toutes les valeurs voulu
      *      dans une chaine de caracteres
      ******************************************************************
      *    Syntaxe:
      *      INSPECT variable REPLACING ALL variable BY variable
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. INSPECT-FIND-REPLACE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           77 STRING1  PIC X(10) VALUE 'QAYHWCNKQK'.
           77 COMPTEUR PIC 9     VALUE ZERO.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY STRING1.

      *    Compter le nombre d'occurence
           INSPECT STRING1 TALLYING COMPTEUR FOR ALL 'Q'
           DISPLAY COMPTEUR.

      *    Remplace toute les occurences de la lettre Q par D
           INSPECT STRING1 REPLACING ALL 'Q' BY 'D'
           DISPLAY STRING1.

            STOP RUN.
       END PROGRAM INSPECT-FIND-REPLACE.
