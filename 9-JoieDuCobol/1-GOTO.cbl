      ******************************************************************
      * Author: David Gilbert
      * Date: 21 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. GOTO.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           77 A PIC 9.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           MOVE 2 TO A.
           GO TO PARAGRAPHE1.

       PARAGRAPHE2.
           DISPLAY A.
           STOP RUN.

       PARAGRAPHE1.
           MOVE 3 TO A.
           GO TO PARAGRAPHE2.

       END PROGRAM GOTO.
