      ******************************************************************
      * Author: Vous
      * Date: 23 mai 2018
      * Purpose: Exercice
      * Tectonics: cobc
      ******************************************************************
      *     Dans cet exercice, vous aller faire un programme en COBOL
      *       qui lit un prix au clavier et affiche
      *       le montant des taxes (en supposant que les taxes sont de
      *       15%) ainsi que le montant total
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. CALCULATRICE-TAXES.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.

           STOP RUN.
       END PROGRAM CALCULATRICE-TAXES.
