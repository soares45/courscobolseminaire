      ******************************************************************
      * Author: David Gilbert
      * Date: 21 mai 2018
      * Purpose: Soustraire 5 de 6 et afficher le signe n�gatif
      * Tectonics: cobc
      ******************************************************************
      * Soustraire 5 de 6 et afficher le signe n�gatif
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. FIXME.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 VALEUR1 PIC 9 VALUE 5.
           77 VALEUR2 PIC 9 VALUE 6.
           77 VALEUR3 PIC 9.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
            SUBTRACT VALEUR2 FROM VALEUR1 GIVING VALEUR3.
            DISPLAY VALEUR3.
            STOP RUN.
       END PROGRAM FIXME.
