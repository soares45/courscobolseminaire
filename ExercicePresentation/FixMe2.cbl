      ******************************************************************
      * Author: David Gilbert
      * Date: 21 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      * Afficher la valeur de A qui doit changer a 3 en allant dans
      *  PARAGRAPHE1
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. FIXME2.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 A PIC 9.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
            MOVE 2 TO A.
            GO TO PARAGRAPHE1.
            DISPLAY A.
            STOP RUN.

       PARAGRAPHE1.
           MOVE 3 TO A.
           GO TO MAIN-PROCEDURE.

       END PROGRAM FIXME2.
