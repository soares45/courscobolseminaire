      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Le programme doit imprimer
      *        FIX ME puis CORRIGER!
      *        en rentrant dans les fonctions.
      *    Indice pour ceux incapable de corriger, utiliser les DISPLAY
      *     mis en commentaire
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. FIXME.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 A PIC 9 VALUE ZERO.
           77 B PIC 9 VALUE ZERO.
           77 C PIC 9 VALUE ZERO.
           77 D PIC 9 VALUE ZERO.
       PROCEDURE DIVISION.
           SET A TO 0.
       MAIN-PROCEDURE.
           DISPLAY "FIX ME".
           PERFORM FONCTION-1 VARYING A FROM 1 BY 3 UNTIL A > 10
           DISPLAY "Corriger!"
           STOP RUN.

       FONCTION-1.
      *    DISPLAY A " " B " " C " " D.
           SET B TO 2.
           COMPUTE C = A + B.
           PERFORM FONCTION-2 VARYING C FROM 2 BY 6 UNTIL C > 7.
           SET A TO 0.


       FONCTION-2.
      *    DISPLAY A " " B " " C " " D.
           SET D TO 1
           COMPUTE B = D + 2.
           PERFORM FONCTION-3 VARYING B FROM 8 BY 1 UNTIL B > 4.

       FONCTION-3.
      *    DISPLAY A " " B " " C " " D.
           SET C TO 0.

       END PROGRAM FIXME.
