# CobolSeminaire

IDE OpenCobolIDE : 
    http://opencobolide.readthedocs.io/en/latest/download.html , 
    https://launchpad.net/cobcide/+download 

Online Compiler  : 
    https://www.tutorialspoint.com/compile_cobol_online.php , 
    https://www.jdoodle.com/execute-cobol-online , 
    https://paiza.io/en/projects/new?language=cobol 
                   
(Je conseil d'utiliser Notepad++ si vous prenez un des online compiler)